<?php
namespace provabdr;

class MyUserClass
{
    
    private $dbconn;
    private $conexao = [
        'host' => 'localhost',
        'user' => 'user',
        'pass' => 'password'
    ];

    private function conecta()
    {
        $this->dbconn = new DatabaseConnection(
            $this->conexao['host'],
            $this->conexao['user'],
            $this->conexao['pass']
        );
    }
    
    public function __construct()
    {
        $this->conecta();
    }

    public function getUserList()
    {
        $results = $this->dbconn->query('select name from user');
        sort($results);
        return $results;
    }
}
