<div class="tarefasTags form">
<?php echo $this->Form->create('TarefasTag'); ?>
	<fieldset>
		<legend><?php echo __('Edit Tarefas Tag'); ?></legend>
	<?php
		echo $this->Form->input('tarefa_id');
		echo $this->Form->input('tag_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('TarefasTag.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('TarefasTag.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Tarefas Tags'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Tarefas'), array('controller' => 'tarefas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tarefa'), array('controller' => 'tarefas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tags'), array('controller' => 'tags', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tag'), array('controller' => 'tags', 'action' => 'add')); ?> </li>
	</ul>
</div>
