#**Prova - PHP  - BDR**

##Questão 01
`/questao01.php`

##Questão 02
`/questao02.php`

##Questão 03
`/MyUserClass.class.php`

##Questão 04
`/questao04-cakephp/`

1. Extrair/Clonar repositório
2. Importar base de dados executando o arquivo `db-tasks.sql`
2. Configurar banco de dados na aplicação *(conforme explicação abaixo)*
3. Abrir a URL correspondente ao caminho da pasta `questao04-cakephp`.

###**Configurar Banco de Dados**
alterar o arquivo `/questao04-cakephp/app/Config/database.php` conforme necessário para a conexão com o banco de dados.

```
class DATABASE_CONFIG {

	public $default = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'root',
		'password' => '',
		'database' => 'db-tasks',
		'encoding' => 'utf8',
	);
}
```


### **Chamadas API**
Foi utilizado a própria [funcionalidade](http://book.cakephp.org/2.0/en/development/rest.html) interna do CakePHP para criar a API REST.

Exemplos:

- para listar as tarefas
    http://caminhodaaplicacao/questao04-cakephp/tarefas/index.xml

- para adicionar tarefa:
    http://caminhodaaplicacao/questao04-cakephp/tarefas/add.xml
    com parâmetros via **POST** `data[Tarefa][titulo]` e `data[Tarefa][descricao]` para título e descrição da tarefa.

- para alterar tarefa:
    http://caminhodaaplicacao/questao04-cakephp/tarefas/edit.xml
    com parâmetros via **POST** `data[Tarefa][titulo]` e `data[Tarefa][descricao]` para título e descrição da tarefa e o campo **`id`** por `data[Tarefa][id]` para saber qual tarefa está alterando.


----------

> **considerações:**
> 1. Desculpa pelo atraso.. tive uns "contra-tempos" no fim de semana;
> 2. Pensei que fosse ter tempo suficiente para utilizar "tags" nas tarefas, mas não deu, então as outras tabelas (além da principal) ficaram inúteis no esquema do bd.
> 3. Perdi muito tempo trabalhando com a interface gráfica, espero que gostem.
