                        
                        <div class="row">
							<div class="col s12">
								<h3 class="header">Lista de Tarefas</h3>
								<ol class="collection" id="lista-tarefas">
									<li class="collection-item">
										<span class="titulo">Sem Tarefas</span>
										<span class="descricao">Lista de tarefas vazia.</span>
									</li>
								</ol>
							</div>
						</div>

						<div id="modal1" class="modal bottom-sheet">
							<div class="modal-content">
                                <form class="col s12" accept-charset="utf-8">
                                    <input type="hidden" name="data[Tarefa][id]" id="TarefaId" />
                                    
									<div class="row">
										<div class="input-field col l3 s12">
											<i class="material-icons prefix">mode_edit</i>
											<input name="data[Tarefa][titulo]" id="TarefaTitulo" type="text" class="validate" length="30" maxlength="30">
											<label for="TarefaTitulo">Título</label>
										</div>
										<div class="input-field col l7 s12">
											<i class="material-icons prefix">textsms</i>
											<input  name="data[Tarefa][descricao]" id="TarefaDescricao" type="text" class="validate" length="100" maxlength="100">
											<label for="TarefaDescricao">Descrição</label>
										</div>
										<div class="input-field col l2 s12 center-align">
                                            <button class="btn waves-effect waves-light" name="action" id="TarefaGravar">Gravar
												<i class="material-icons right">save</i>
											</button>
										</div>
									</div>
								</form>
                                
							</div>
						</div>
						<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
                            <a class="btn-floating btn-large waves-effect waves-light" href="#" id="TarefaNovo">
								<i class="large material-icons">add</i>
							</a>
						</div>
