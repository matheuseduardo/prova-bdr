<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Tag'), ['action' => 'edit', $tag->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Tag'), ['action' => 'delete', $tag->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tag->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Tags'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tag'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tarefas'), ['controller' => 'Tarefas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tarefa'), ['controller' => 'Tarefas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tags view large-9 medium-8 columns content">
    <h3><?= h($tag->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Descricao') ?></th>
            <td><?= h($tag->descricao) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($tag->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Tarefas') ?></h4>
        <?php if (!empty($tag->tarefas)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Titulo') ?></th>
                <th><?= __('Descricao') ?></th>
                <th><?= __('Prioridade') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($tag->tarefas as $tarefas): ?>
            <tr>
                <td><?= h($tarefas->id) ?></td>
                <td><?= h($tarefas->titulo) ?></td>
                <td><?= h($tarefas->descricao) ?></td>
                <td><?= h($tarefas->prioridade) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Tarefas', 'action' => 'view', $tarefas->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Tarefas', 'action' => 'edit', $tarefas->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Tarefas', 'action' => 'delete', $tarefas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tarefas->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
