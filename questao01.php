<?php
echo '<pre>'; // para uma melhor exibição

for ($numero=1; $numero <= 100; $numero++) {
    if ($numero%15==0) {
        print "FizzBuzz\r\n";
        continue;
    }
    if ($numero%3==0) {
        print "Fizz\r\n";
        continue;
    }
    if ($numero%5==0) {
        print "Buzz\r\n";
        continue;
    }
    
    print "$numero\r\n";
}
