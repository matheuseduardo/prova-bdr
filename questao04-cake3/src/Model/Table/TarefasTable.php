<?php
namespace App\Model\Table;

use App\Model\Entity\Tarefa;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tarefas Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Tags
 */
class TarefasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tarefas');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->belongsToMany('Tags', [
            'foreignKey' => 'tarefa_id',
            'targetForeignKey' => 'tag_id',
            'joinTable' => 'tarefas_tags'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('titulo', 'create')
            ->notEmpty('titulo');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->add('prioridade', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('prioridade');

        return $validator;
    }
}
