<?php
App::uses('AppController', 'Controller');
/**
 * TarefasTags Controller
 *
 * @property TarefasTag $TarefasTag
 * @property PaginatorComponent $Paginator
 */
class TarefasTagsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->TarefasTag->recursive = 0;
		$this->set('tarefasTags', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->TarefasTag->exists($id)) {
			throw new NotFoundException(__('Invalid tarefas tag'));
		}
		$options = array('conditions' => array('TarefasTag.' . $this->TarefasTag->primaryKey => $id));
		$this->set('tarefasTag', $this->TarefasTag->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->TarefasTag->create();
			if ($this->TarefasTag->save($this->request->data)) {
				$this->Session->setFlash(__('The tarefas tag has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The tarefas tag could not be saved. Please, try again.'));
			}
		}
		$tarefas = $this->TarefasTag->Tarefa->find('list');
		$tags = $this->TarefasTag->Tag->find('list');
		$this->set(compact('tarefas', 'tags'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->TarefasTag->exists($id)) {
			throw new NotFoundException(__('Invalid tarefas tag'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->TarefasTag->save($this->request->data)) {
				$this->Session->setFlash(__('The tarefas tag has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The tarefas tag could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('TarefasTag.' . $this->TarefasTag->primaryKey => $id));
			$this->request->data = $this->TarefasTag->find('first', $options);
		}
		$tarefas = $this->TarefasTag->Tarefa->find('list');
		$tags = $this->TarefasTag->Tag->find('list');
		$this->set(compact('tarefas', 'tags'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->TarefasTag->id = $id;
		if (!$this->TarefasTag->exists()) {
			throw new NotFoundException(__('Invalid tarefas tag'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->TarefasTag->delete()) {
			$this->Session->setFlash(__('The tarefas tag has been deleted.'));
		} else {
			$this->Session->setFlash(__('The tarefas tag could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
