<?php

App::uses('AppController', 'Controller');

/**
 * Tarefas Controller
 *
 * @property Tarefa $Tarefa
 * @property PaginatorComponent $Paginator
 */
class TarefasController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'RequestHandler');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Tarefa->recursive = 0;

        $tarefas = $this->Tarefa->find('all', array(
            'order' => array('Tarefa.prioridade' => 'asc')
        ));

        $this->set('tarefas', $tarefas);
        $this->set('_serialize', array('tarefas'));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Tarefa->exists($id)) {
            throw new NotFoundException(__('Invalid tarefa'));
        }
        $options = array('conditions' => array('Tarefa.' . $this->Tarefa->primaryKey => $id));
        $this->set('tarefa', $this->Tarefa->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is(array('post', 'ajax', 'xml', 'json'))) {

            $this->Tarefa->create();

            $p_max = $this->Tarefa->find('first', array(
                'fields' => 'ifnull(max(prioridade)+1,0) as maxp',
                'recursive' => 0
            ));

            $this->request->data['Tarefa']['prioridade'] = $p_max[0]['maxp'];


            if ($this->Tarefa->save($this->request->data)) {
                $resultado = [
                    "sucesso" => true,
                    "mensagem" => "Nova tarefa gravada com sucesso"
                ];
            } else {
                $resultado = [
                    "sucesso" => false,
                    "mensagem" => "Erro ao gravar."
                ];
            }

            if ($this->request->is('ajax')) {
                echo json_encode($resultado);
                exit;
            }

            $this->set('resultado', $resultado);
            $this->set('_serialize', array('resultado'));
        }
    }

    /**
     * listaTarefas method
     *
     * @return void
     */
    public function listaTarefas() {
        $this->Tarefa->recursive = 0;
        $tarefas = $this->Tarefa->find('all', array(
            'fields' => array('Tarefa.id, Tarefa.titulo, Tarefa.descricao, Tarefa.prioridade'),
            'order' => array('Tarefa.prioridade' => 'asc'),
            'recursive' => 0,
        ));
        $tarefas = $this->trata_array_tarefas($tarefas);
        echo json_encode($tarefas);
        exit;
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Tarefa->exists($id)) {
            throw new NotFoundException(__('Invalid tarefa'));
        }
        if ($this->request->is(array('post', 'put', 'ajax', 'xml', 'json'))) {

            if ($this->Tarefa->save($this->request->data)) {
                $resultado = [
                    "sucesso" => true,
                    "mensagem" => "Tarefa alterada com sucesso"
                ];
            } else {
                $resultado = [
                    "sucesso" => false,
                    "mensagem" => "Erro ao gravar."
                ];
            }

            if ($this->request->is('ajax')) {
                echo json_encode($resultado);
                exit;
            }

            $this->set('resultado', $resultado);
            $this->set('_serialize', array('resultado'));
        }
    }

    /**
     * definePrioridade method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function atualizaPrioridade($id = null, $prioridade = null) {
        if (!$this->Tarefa->exists($id)) {
            $resultado = [
                "sucesso" => false,
                "mensagem" => "Id de tarefa inexistente"
            ];

            
        } else {

            $p_atual = $this->Tarefa->find('first', array(
                'fields' => 'prioridade',
                'conditions' => array('Tarefa.id' => $id),
                'recursive' => 0
            ));

            $p_atual = $p_atual['Tarefa']['prioridade'];

            if ($prioridade < $p_atual) {
                $q = " update tarefas set prioridade = (prioridade+1) "
                        . " where prioridade >= " . $prioridade
                        . "   and prioridade < " . $p_atual;

                $this->Tarefa->query($q);
            }

            if ($prioridade > $p_atual) {
                $q = " update tarefas set prioridade = (prioridade-1) "
                        . " where prioridade <= " . $prioridade
                        . "   and prioridade > " . $p_atual;

                $this->Tarefa->query($q);
            }


            $q = " update tarefas set prioridade = " . $prioridade
                    . " where id = " . $id;
            $this->Tarefa->query($q);

            $resultado = [
                "sucesso" => true,
                "mensagem" => "Tarefa atualizada com sucesso $id"
            ];

            if ($this->request->is('ajax')) {
                echo json_encode($resultado);
                exit;
            }

            
        }
        set('resultado', $resultado);
        set('_serialize', array('resultado'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Tarefa->id = $id;
        if (!$this->Tarefa->exists()) {
            throw new NotFoundException(__('Invalid tarefa'));
        }
        $this->request->allowMethod('post', 'delete', 'ajax');

        $p_atual = $this->Tarefa->find('first', array(
            'fields' => 'prioridade',
            'conditions' => array('Tarefa.id' => $id),
            'recursive' => 0
        ));
        $p_atual = $p_atual['Tarefa']['prioridade'];


        if ($this->Tarefa->delete()) {
            
            $q = " update tarefas set prioridade = (prioridade-1) "
                    . " where prioridade > " . $p_atual;
            $this->Tarefa->query($q);

            if ($this->request->is('ajax')) {
                $resultado = [
                    "sucesso" => true,
                    "mensagem" => "Tarefa excluída com sucesso"
                ];
                echo json_encode($resultado);
                exit;
            }
            
        } else {
            
        }
        set('resultado', $resultado);
        set('_serialize', array('resultado'));
    }

    public function trata_array_tarefas($arr) {

        $retorno = [];

        foreach ($arr as $reg):
            $retorno[] = [
                'id' => $reg['Tarefa']['id'],
                'prioridade' => $reg['Tarefa']['prioridade'],
                'titulo' => $reg['Tarefa']['titulo'],
                'descricao' => $reg['Tarefa']['descricao'],
            ];
        endforeach;

        return [
            'resultado' => $retorno
        ];
    }

}
