<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Tarefas Tag'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tarefas'), ['controller' => 'Tarefas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tarefa'), ['controller' => 'Tarefas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tags'), ['controller' => 'Tags', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tag'), ['controller' => 'Tags', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="tarefasTags index large-9 medium-8 columns content">
    <h3><?= __('Tarefas Tags') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('tarefa_id') ?></th>
                <th><?= $this->Paginator->sort('tag_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tarefasTags as $tarefasTag): ?>
            <tr>
                <td><?= $tarefasTag->has('tarefa') ? $this->Html->link($tarefasTag->tarefa->id, ['controller' => 'Tarefas', 'action' => 'view', $tarefasTag->tarefa->id]) : '' ?></td>
                <td><?= $tarefasTag->has('tag') ? $this->Html->link($tarefasTag->tag->id, ['controller' => 'Tags', 'action' => 'view', $tarefasTag->tag->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $tarefasTag->tarefa_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $tarefasTag->tarefa_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $tarefasTag->tarefa_id], ['confirm' => __('Are you sure you want to delete # {0}?', $tarefasTag->tarefa_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
