<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Tarefas Tag'), ['action' => 'edit', $tarefasTag->tarefa_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Tarefas Tag'), ['action' => 'delete', $tarefasTag->tarefa_id], ['confirm' => __('Are you sure you want to delete # {0}?', $tarefasTag->tarefa_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Tarefas Tags'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tarefas Tag'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tarefas'), ['controller' => 'Tarefas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tarefa'), ['controller' => 'Tarefas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tags'), ['controller' => 'Tags', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tag'), ['controller' => 'Tags', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tarefasTags view large-9 medium-8 columns content">
    <h3><?= h($tarefasTag->tarefa_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Tarefa') ?></th>
            <td><?= $tarefasTag->has('tarefa') ? $this->Html->link($tarefasTag->tarefa->id, ['controller' => 'Tarefas', 'action' => 'view', $tarefasTag->tarefa->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Tag') ?></th>
            <td><?= $tarefasTag->has('tag') ? $this->Html->link($tarefasTag->tag->id, ['controller' => 'Tags', 'action' => 'view', $tarefasTag->tag->id]) : '' ?></td>
        </tr>
    </table>
</div>
