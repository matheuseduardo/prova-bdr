<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $tarefasTag->tarefa_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $tarefasTag->tarefa_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Tarefas Tags'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Tarefas'), ['controller' => 'Tarefas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tarefa'), ['controller' => 'Tarefas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tags'), ['controller' => 'Tags', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tag'), ['controller' => 'Tags', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="tarefasTags form large-9 medium-8 columns content">
    <?= $this->Form->create($tarefasTag) ?>
    <fieldset>
        <legend><?= __('Edit Tarefas Tag') ?></legend>
        <?php
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
