<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TarefasTags Controller
 *
 * @property \App\Model\Table\TarefasTagsTable $TarefasTags
 */
class TarefasTagsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Tarefas', 'Tags']
        ];
        $this->set('tarefasTags', $this->paginate($this->TarefasTags));
        $this->set('_serialize', ['tarefasTags']);
    }

    /**
     * View method
     *
     * @param string|null $id Tarefas Tag id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tarefasTag = $this->TarefasTags->get($id, [
            'contain' => ['Tarefas', 'Tags']
        ]);
        $this->set('tarefasTag', $tarefasTag);
        $this->set('_serialize', ['tarefasTag']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tarefasTag = $this->TarefasTags->newEntity();
        if ($this->request->is('post')) {
            $tarefasTag = $this->TarefasTags->patchEntity($tarefasTag, $this->request->data);
            if ($this->TarefasTags->save($tarefasTag)) {
                $this->Flash->success(__('The tarefas tag has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The tarefas tag could not be saved. Please, try again.'));
            }
        }
        $tarefas = $this->TarefasTags->Tarefas->find('list', ['limit' => 200]);
        $tags = $this->TarefasTags->Tags->find('list', ['limit' => 200]);
        $this->set(compact('tarefasTag', 'tarefas', 'tags'));
        $this->set('_serialize', ['tarefasTag']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tarefas Tag id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tarefasTag = $this->TarefasTags->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tarefasTag = $this->TarefasTags->patchEntity($tarefasTag, $this->request->data);
            if ($this->TarefasTags->save($tarefasTag)) {
                $this->Flash->success(__('The tarefas tag has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The tarefas tag could not be saved. Please, try again.'));
            }
        }
        $tarefas = $this->TarefasTags->Tarefas->find('list', ['limit' => 200]);
        $tags = $this->TarefasTags->Tags->find('list', ['limit' => 200]);
        $this->set(compact('tarefasTag', 'tarefas', 'tags'));
        $this->set('_serialize', ['tarefasTag']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tarefas Tag id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tarefasTag = $this->TarefasTags->get($id);
        if ($this->TarefasTags->delete($tarefasTag)) {
            $this->Flash->success(__('The tarefas tag has been deleted.'));
        } else {
            $this->Flash->error(__('The tarefas tag could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
