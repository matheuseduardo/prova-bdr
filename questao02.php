<?php
@session_start(); // não sei se a sessão está configurada para "auto start".
                  // se sim, pelo menos com o '@' não produziria erro.

if ((isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'] === true) ||
        (isset($_COOKIE['loggedin']) && $_COOKIE['loggedIn'] === true)) {
    header("Location: http://www.google.com");
    exit();
}
