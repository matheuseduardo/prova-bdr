<div class="tarefasTags view">
<h2><?php echo __('Tarefas Tag'); ?></h2>
	<dl>
		<dt><?php echo __('Tarefa'); ?></dt>
		<dd>
			<?php echo $this->Html->link($tarefasTag['Tarefa']['id'], array('controller' => 'tarefas', 'action' => 'view', $tarefasTag['Tarefa']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tag'); ?></dt>
		<dd>
			<?php echo $this->Html->link($tarefasTag['Tag']['id'], array('controller' => 'tags', 'action' => 'view', $tarefasTag['Tag']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Tarefas Tag'), array('action' => 'edit', $tarefasTag['TarefasTag']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Tarefas Tag'), array('action' => 'delete', $tarefasTag['TarefasTag']['id']), array(), __('Are you sure you want to delete # %s?', $tarefasTag['TarefasTag']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Tarefas Tags'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tarefas Tag'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tarefas'), array('controller' => 'tarefas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tarefa'), array('controller' => 'tarefas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tags'), array('controller' => 'tags', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tag'), array('controller' => 'tags', 'action' => 'add')); ?> </li>
	</ul>
</div>
