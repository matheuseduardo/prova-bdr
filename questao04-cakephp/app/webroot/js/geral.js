
$(document).ready(function () {
	$('#lista-tarefas').sortable({
		stop: function (event, ui) {
			
				tarefa = ui.item.find('input[type=hidden]').get(0).value;
				prioridade = ui.item.index();
	
				atualizaPrioridade(tarefa, prioridade);
			
		},
	});
	$('#lista-tarefas').disableSelection();


	$('#TarefaNovo').on('click', function (e) {
		e.preventDefault();
		$('#modal1').openModal();
		$('#modal1 form #TarefaId').val('');
	})

	$('#TarefaGravar').on('click', function (e) {

		e.preventDefault();
		id = $('#modal1 form #TarefaId').val();
		if ($.trim(id) == '') {
			adicionarTarefa();
		}
		else {
			gravarEdicaoTarefa(id);
		}
	})

	carregaTarefas();

});

function carregaTarefas() {

	$.getJSON($URLSISTEMA + 'tarefas/listaTarefas', function (data, txtStatus) {

		if (data.resultado.length > 0) {
			$('ol#lista-tarefas').html('');

			$.each(data.resultado, function (i, registro) {
				divsec = '<div class="secondary-content"><a href="javascript:editarTarefa(' + registro.id + ')"><i class="material-icons">edit</i></a>';
				divsec += '<a href="javascript:excluirTarefa(' + registro.id + ')" title="Exluir tarefa"><i class="material-icons">done</i></a></div>'
				$('<li/>', {"class": 'collection-item', "id": 'reg' + registro.id})
						.append('<span class="titulo">' + registro.titulo + '</span>')
						.append('<span class="descricao">' + registro.descricao + '</span>')
						.append(divsec)
						.append('<input type="hidden" name="id" value="' + registro.id + '">')
						.appendTo('ol#lista-tarefas');

			})

		}
		else {
			$('ol#lista-tarefas').html('<li class="collection-item">Sem tarefas cadastradas</li>');
		}
		
		
		Materialize.showStaggeredList('ol#lista-tarefas');

	});

}

function atualizaPrioridade(t, p) {
	var cfg = {
		'method': 'GET',
		'url': $URLSISTEMA + 'tarefas/atualizaPrioridade/' + t + '/' + p,
		'dataType': 'json',
		'success': function (data) {
			if (data.sucesso) {

			}
		},
		'error': function () {
			location.reload();
		}
	}

	$.ajax(cfg);
}


function excluirTarefa(id) {

	if (confirm('Confirmar item como concluído e excluí-lo da lista de tarefas?')) {

		var cfg = {
			'method': 'GET',
			'url': $URLSISTEMA + 'tarefas/delete/' + id,
			'dataType': 'json',
			'success': function (data) {
				if (data.sucesso) {
					Materialize.toast(data.mensagem, 4000)
					carregaTarefas();
				}
				carregaTarefas()
			},
			'error': function () {
				//location.reload();
			}
		}

		$.ajax(cfg);
	}
}

function adicionarTarefa() {

	var cfg = {
		'method': 'POST',
		'url': $URLSISTEMA + 'tarefas/add',
		'data': {
			'data[Tarefa][titulo]': $('#TarefaTitulo').val(),
			'data[Tarefa][descricao]': $('#TarefaDescricao').val()
		},
		'dataType': 'json',
		'success': function (data) {
			if (data.sucesso) {
				Materialize.toast(data.mensagem, 4000)
				carregaTarefas();
			}
			carregaTarefas();
		},
		'error': function () {

		},
		'complete': function () {
			$('#modal1').closeModal();
		}
	}
	$.ajax(cfg);
}

function editarTarefa(id) {
	$('#modal1').openModal({complete: limpaForm});
	$('#modal1 form #TarefaDescricao').val($('#reg' + id).find('.descricao').text()).addClass('active').focus();
	$('#modal1 form #TarefaTitulo').val($('#reg' + id).find('.titulo').text()).addClass('active').focus();
	$('#modal1 form #TarefaId').val(id);

}

function gravarEdicaoTarefa(id) {

	var cfg = {
		'method': 'POST',
		'url': $URLSISTEMA + 'tarefas/edit/' + id,
		'data': {
			'data[Tarefa][id]': $('#TarefaId').val(),
			'data[Tarefa][titulo]': $('#TarefaTitulo').val(),
			'data[Tarefa][descricao]': $('#TarefaDescricao').val()
		},
		'dataType': 'json',
		'success': function (data) {
			if (data.sucesso) {
				Materialize.toast(data.mensagem, 4000)
				carregaTarefas();
			}
		},
		'error': function () {

		},
		'complete': function () {
			$('#modal1').closeModal();
			limpaForm();
		}
	}

	$.ajax(cfg);
}

function limpaForm() {
	$('#modal1 form #TarefaTitulo').val('');
	$('#modal1 form #TarefaDescricao').val('');
	$('#modal1 form #TarefaId').val('');
}
	